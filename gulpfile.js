/**
 * This file can be overwritten with some scripts of generator-jive-react-addon. Please don't edit
 * it if you're planning to add middlewares later
 */

const {series} = require('gulp')

const {
    copyHTML,
    runWebpack,
    jiveBuildApp,
    rename,
    copyHTMLLocal,
    copyHTMLHot,
    runWebpackServer,
    getBuild,
    config
} = require('./build-scripts/gulp/main')

/**
 * START TASKS
 */

exports.default = series(getBuild, copyHTML, config, runWebpack, jiveBuildApp, rename)

exports["local"] = series(getBuild, copyHTMLLocal, runWebpack)

exports["local-hot"] = series(copyHTMLHot, runWebpackServer)