import './helpers/polyfills'
import './helpers/addonPropsLog'
import './main.scss'

import jive from 'jive'
import render from './render'
import resizeMe from './helpers/resizeMe'
import {prepareConfig} from './helpers/dataUtils'
import TileView from './view/components/TileView'

import {DEFAULT_CONFIG} from './constants/configDefaults'
import {CONVERTERS} from './constants/converters'

jive.tile.onOpen(async function (config, options) {
    try {
        resizeMe()

        const newConfig = await prepareConfig(config, DEFAULT_CONFIG, CONVERTERS)

        // binding react to DOM
        render(TileView, newConfig)

        if (module.hot) {
            module.hot.accept('./view/components/TileView', () => {
                render(TileView, newConfig)
                resizeMe()
            })
        }

    } catch (e) {
        console.error('jive.tile.onOpen ERROR', e)
        console.error(e.stack)
    }
});