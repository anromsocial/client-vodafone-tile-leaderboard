import './helpers/polyfills'
import './main.scss'

import jive from 'jive'
import render from './render'
import resizeMe from './helpers/resizeMe'
import {prepareConfig} from './helpers/dataUtils'
import {sendAction} from './helpers/DomActions'
import TileConfiguration from './configuration/components/TileConfiguration'

import {DEFAULT_CONFIG} from './constants/configDefaults'
import CONVERTERS from './constants/converters'

jive.tile.onOpen(async function (config = {}, options) {
    try {
        resizeMe()

        window.parent.document.querySelector('.js-embed').style.maxHeight= '2500px';

        await sendAction(process.env.APP_NAME, 'dom_actions.html', 'disable_popup_closing')

        const newConfig = await prepareConfig(config.data || {}, DEFAULT_CONFIG, CONVERTERS)

        // binding react to DOM
        render(TileConfiguration, newConfig)

        if (module.hot) {
            module.hot.accept(
                './configuration/components/TileConfiguration',
                () => {
                    render(TileConfiguration, newConfig)
                    resizeMe()
                }
            )
        }

    } catch (e) {
        console.error('jive.tile.onOpen ERROR', e);
        console.error(e.stack);
    }
});
