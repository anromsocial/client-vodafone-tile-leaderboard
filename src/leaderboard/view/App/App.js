import React, { useContext } from 'react';

import { configContext } from '../components/Context/ConfigCotext';

import LeaderBoard from '../components/LeaderBoard/LeaderBoard';

import { formateUserForConfigurate } from '../../helpers/changeToRealiveLinks';

import './App.css';

const App = () => {
  const { boards, customHeaderTile } = useContext(configContext);
  const formatedUserFielsForBoards = formateUserForConfigurate(boards, false);
  return (
    <div style={{ userSelect: 'none' }}>
      <div className='customHeader'>{customHeaderTile}</div>
      {boards.length > 0 && formatedUserFielsForBoards.map(board => <LeaderBoard key={board.id} board={board} />)}
    </div>
  );
};

export default App;
