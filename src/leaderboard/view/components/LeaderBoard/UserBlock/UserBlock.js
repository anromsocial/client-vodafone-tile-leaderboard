import React, { useState, useEffect } from 'react';
import url from 'url'

import resizeMe from '../../../../helpers/resizeMe';

import defaultLogo from './default-profile.png';

import './UserBlock.css';

const UserBlock = ({ config }) => {
  const { users, isUserNumber, backgroundColor, color, userQuantity, uploadUserQuantity, width } = config;

  const [amountShow, setAmountShow] = useState(userQuantity);

  const onShowMore = () => {
    if (users.length <= amountShow) return;
    setAmountShow(prevState => prevState + uploadUserQuantity);
  };

  const [styleWidth, setStyleWidth] = useState(width);

  useEffect(() => {
    let updateWidth;
    switch (width) {
      case 1140:
        updateWidth = isUserNumber ? '58%' : '70%';
        break;

      case 800:
        updateWidth = isUserNumber ? '60%' : '70%';
        break;

      case 460:
        updateWidth = '72%';
        break;

      case 300:
        updateWidth = isUserNumber ? '53%' : '70%';
        break;

      default:
        updateWidth = isUserNumber ? '53%' : '70%';
        break;
    }
    setStyleWidth(updateWidth);
  }, [width]);

  useEffect(() => console.log('uploadUserQuantity', uploadUserQuantity), [uploadUserQuantity]);

  useEffect(() => console.log('amountShow', amountShow), [amountShow]);

  useEffect(() => resizeMe());

  return (
    <>
      <div className='usersList'>
        {users.length > 0 &&
          users.slice(0, amountShow).map(({ userLogo, userName, userPosition, userId, userProfileLink = 'javascript:void(0)', userNumber }, idx) => {
            const urlObj = url.parse(userProfileLink)

            //WARNING! location.hostname in tile will only match top window hostname if tile
            // domain security is off which is true for VF but may not be so in other instances!
            // Copy-paste only with this in mind!
            const isInternalLink = (urlObj.pathname && urlObj.pathname.indexOf('/people/') === 0)
              && (!urlObj.hostname || urlObj.hostname === window.location.hostname)

            const isEmptyLink = userProfileLink === '';
            const setTarget = '_blank' // (isEmptyLink || isInternalLink) ? '_top' : '_blank';

            const currentUserNumber = !!userNumber ? userNumber : idx + 1;
            
            const userInfoWidth = userBlockWidth => {
              const userNumberLength = currentUserNumber.toString().length;
              if (width === 800 && userNumberLength === 2 && isUserNumber) {
                return '52%';
              }
              return userBlockWidth;
            }
            return (
              <a
                key={userId}
                href={userProfileLink}
                className='jiveTT-hover-user userLink'
                style={{ cursor: isEmptyLink ? 'default' : 'pointer' }}
                target={setTarget}
              >
                <div className='mainBlock' style={{ backgroundColor, color }}>
                  <img src={userLogo || defaultLogo} className='userLogo' alt={userName} />

                  <div className='userInfo' style={{ width: userInfoWidth(styleWidth) }}>
                    <div className='userName'>{userName}</div>
                    <div className='userPosition'>{userPosition}</div>
                  </div>

                  {isUserNumber && <div className='userNumber'>{currentUserNumber}</div>}
                </div>
              </a>
            );
          })}
      </div>

      {users.length > amountShow && (
        <div style={{ textAlign: 'center' }}>
          <div onClick={onShowMore} className='showMore'>
            Показати ще
          </div>
        </div>
      )}
    </>
  );
};

export default UserBlock;
