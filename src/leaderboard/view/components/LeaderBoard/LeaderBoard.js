import React, { useState, useEffect } from 'react';

import HeaderBlock from './HeaderBlock/HeaderBlock';
import RaitingBlock from './RaitingBlock/RaitingBlock';

import resizeMe from '../../../helpers/resizeMe';
import { hexRgba } from '../../../helpers/hexRgba';

import './LeaderBoard.css';

const LeaderBoard = ({ board }) => {
  const [isOpen, setOpen] = useState(false);
  const onHandleOpen = () => setOpen(prevState => !prevState);

  const width = window.document.body.offsetWidth;

  useEffect(() => resizeMe());

  const {
    backgroundColorHeader,
    backgroundColorRaitingBlock,
    backgroundColorUserBlock,
    imgSrc,
    isUserNumber,
    subtitle,
    textColorHeader,
    textColorUserBlock,
    title,
    userQuantity,
    uploadUserQuantity,
    users,
  } = board;

  const headerBlockConfig = {
    title,
    subtitle,
    imgSrc,
    backgroundColor: hexRgba(backgroundColorHeader),
    color: hexRgba(textColorHeader),
    width,
  };

  const raintingBlockConfig = {
    backgroundColor: hexRgba(backgroundColorRaitingBlock),
  };

  const userBlockConfig = {
    users,
    isUserNumber,
    backgroundColor: hexRgba(backgroundColorUserBlock),
    color: hexRgba(textColorUserBlock),
    userQuantity: Number(userQuantity),
    uploadUserQuantity: Number(uploadUserQuantity),
    width,
  };

  return (
    <div className='leaderBoard'>
      <HeaderBlock onHandleClick={onHandleOpen} config={headerBlockConfig} isOpenBlock={isOpen} />
      {isOpen && <RaitingBlock config={raintingBlockConfig} userBlockConfig={userBlockConfig} />}
    </div>
  );
};

export default LeaderBoard;
