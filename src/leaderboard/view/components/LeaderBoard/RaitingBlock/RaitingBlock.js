import React from 'react';

import UserBlock from '../UserBlock/UserBlock';

import './RaitingBlock.css';

const RaitingBlock = ({ config, userBlockConfig }) => {
  const { backgroundColor } = config;

  return (
    <div className='raitingBlock' style={{ backgroundColor }}>
      <UserBlock config={userBlockConfig} />
    </div>
  );
};

export default RaitingBlock;
