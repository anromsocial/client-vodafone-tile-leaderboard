import React, { useState, useEffect, useContext } from 'react';
import TextTruncate from 'react-text-truncate';

import { configContext } from '../../../components/Context/ConfigCotext';

import './HeaderBlock.css';

const HeaderBlock = ({ onHandleClick, config, isOpenBlock }) => {
  const { title, subtitle, imgSrc, backgroundColor, color, width } = config;

  const [line, setLine] = useState(1);

  const { isShowImageInCollapseMode } = useContext(configContext);

  const [isShowImg, setShowImg] = useState(isShowImageInCollapseMode);

  const isMobileShowImgInCollapseMode = width <= 459 && !isOpenBlock ? false : true;

  useEffect(() => {
    let isShowImg;
    if (isOpenBlock) {
      isShowImg = true;
    } else {
      isShowImageInCollapseMode ? (isShowImg = true) : (isShowImg = false);
    }
    setShowImg(isShowImg);
  }, [isOpenBlock]);

  useEffect(() => {
    let countLine;
    switch (width) {
      case 300:
        countLine = 3;
        break;
      case 460:
        countLine = 2;
        break;
      case 800:
        countLine = 2;
        break;
      case 1140:
        countLine = 1;
        break;
      default:
        countLine = 3;
        break;
    }

    setLine(countLine);
  }, [width]);

  return (
    <div className='headerBlock' onClick={onHandleClick} style={{ backgroundColor, color }}>
      <div className='titleBlock'>
        <TextTruncate line={line} truncateText={' ...'} text={title.toUpperCase()} />
      </div>

      {subtitle !== '' && isOpenBlock && (
        <div className='subTitleBlock'>
          <TextTruncate line={line} truncateText={' ...'} text={subtitle.toUpperCase()} />
        </div>
      )}
      {imgSrc !== '' && isShowImg && isMobileShowImgInCollapseMode && <img className='boardLogo' src={imgSrc} />}
    </div>
  );
};

export default HeaderBlock;
