import React, { createContext } from 'react'

export const configContext = createContext();

export const ConfigContextProvider = ({ config, children }) => (
    <configContext.Provider value={config}>
        {children}
    </configContext.Provider>
);