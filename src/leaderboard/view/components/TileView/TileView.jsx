import React, { useEffect } from 'react';

import { ConfigContextProvider } from '../Context/ConfigCotext';

import App from '../../App/App';

import FontsLoader from '../../../helpers/FontsLoader';

const TileView = ({ config }) => {
  useEffect(() => console.log('INITIAL CONFIGS', config), []);
  return (
    <ConfigContextProvider config={config}>
      <FontsLoader />
      <App />
    </ConfigContextProvider>
  );
};

export default TileView;
