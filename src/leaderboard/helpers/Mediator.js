export default class Mediator {

    constructor(){
        this.subscriptions = {}
    }

    on(eventName, callback){
        if(!this.subscriptions[eventName]){
            this.subscriptions[eventName] = []
        }

        this.subscriptions[eventName].push(callback)
    }

    async trigger(eventName, data){
        if (this.subscriptions[eventName]){
            const promises = this.subscriptions[eventName].map(callback => {

                const ret = callback(data)

                return ret.then ? ret : Promise.resolve(ret)
            })

            //console.log('promises', promises)

            return await Promise.all(promises)
        }

        return false
    }

    off(eventName, callback){
        if (this.subscriptions[eventName]){
            const cbIndex = this.subscriptions[eventName].findIndex(cb => cb === callback)

            console.log('cbIndex', cbIndex)

            if (cbIndex !== -1){
                this.subscriptions[eventName].splice(cbIndex, 1)
            }

            if (!this.subscriptions[eventName].length){
                delete this.subscriptions[eventName]
            }
        }
    }
}