import { Position, Toaster } from '@blueprintjs/core';

export const ToasterSuccess = Toaster.create({
  className: 'toaster toaster-success',
  position: Position.BOTTOM,
});

export const ToasterWarning = Toaster.create({
  className: 'toaster toaster-warning',
  position: Position.BOTTOM,
});

export const ToasterError = Toaster.create({
  className: 'toaster toaster-error',
  position: Position.BOTTOM,
});
