/**
 * Created by M. Yegorov on 2016-11-03.
 */

import React from 'react';
import { tilePath } from 'anrom-jive-app-tools/tileProps';

//importing fonts to place them in folder
//indert your fonts
import VF_Lt_WOFF from '../fonts/VodafoneLt.woff';
import VF_Rg_WOFF from '../fonts/VodafoneRg.woff';
import VF_RgBd_WOFF from '../fonts/VodafoneRgBd.woff';
import VF_ExB_WOFF from '../fonts/VodafoneExB.woff';

export default function FontLoader() {
  return (
    <style>{`
        @font-face {
            font-family: 'VodafoneLt';
            font-weight: 300;
            src: url(${tilePath()}/fonts/${VF_Lt_WOFF}) format('woff');
        }

        @font-face {
            font-family: 'VodafoneRg';
            font-weight: 400;
            src: url(${tilePath()}/fonts/${VF_Rg_WOFF}) format('woff');
        }

        @font-face {
            font-family: 'VodafoneRgBd';
            font-weight: 600;
            src: url(${tilePath()}/fonts/${VF_RgBd_WOFF}) format('woff');
        }

        @font-face {
            font-family: 'VodafoneExB';
            font-weight: 900;
            src: url(${tilePath()}/fonts/${VF_ExB_WOFF}) format('woff');
        }       
        `}</style>
  );
}
