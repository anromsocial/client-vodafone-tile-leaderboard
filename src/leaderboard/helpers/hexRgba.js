import hexToRgba from 'hex-to-rgba';

export const hexRgba = colorObj => {
    if(typeof(colorObj) === 'object') {
        const { color, alpha } = colorObj;
        return hexToRgba(color, alpha/100);
    };
    return colorObj;
};