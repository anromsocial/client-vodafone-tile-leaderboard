const DEV = process.env.NODE_ENV === 'development'
console.info('=============================')
console.info('%c'+process.env.APP_NAME, "font-weight:bold;color:black")
console.info('env.: %c'+process.env.NODE_ENV, DEV ? "color:red" : "color:blue")
console.info('version: ' + process.env.VERSION, 'built at', __BUILD_DATE__)
if (__GIT__){
    if (__GIT__ !== 'ok') {
        console.info(__GIT_COMMENT__)
    } else {
        if (__GIT_STATUS__ === 'OK') {
            console.info(
                'commit', __GIT_REVISION__, 'on branch', __GIT_BRANCH__ + ';',
                'built by', __GIT_USER_NAME__, '(' + __GIT_USER_EMAIL__ + ')'
            )
        } else {
            console.warn(
                'Attention! Package build by', __GIT_USER_NAME__, '(' + __GIT_USER_EMAIL__ + ')',
                'without committing latest changes to git'
            )
            console.warn('latest commit is', __GIT_REVISION__, 'on branch', __GIT_BRANCH__)
        }
    }
}
console.info('=============================')