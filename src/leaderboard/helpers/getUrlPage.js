function getQueryParams(qs) {
  qs = qs.split('+').join(' ');
  var params = {},
    tokens,
    re = /[?&]?([^=]+)=([^&]*)/g;
  while ((tokens = re.exec(qs))) {
    params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
  }
  return params;
}

export async function getPageUrl() {
  let pageId = getQueryParams(parent.window.location.search).pageId;
  return jQuery.get('/api/core/v3/pages/' + pageId);
}
