export default function reload(e) {
    if ((e.ctrlKey || e.metaKey) && e.altKey && process.env.NODE_ENV === 'development') {
        window.location.reload()
    }
}