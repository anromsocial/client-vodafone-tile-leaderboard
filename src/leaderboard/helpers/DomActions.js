function guid() {
    const s4 = () => Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function buildUrl(tileName, page, parameter, value) {
    // tile base url where hosted this tile on root domain
    const basePublicUrl = /(.+tiles)/gmi.exec(gadgets.util.getUrlParameters().url)[1];

    return `${basePublicUrl}/${tileName}/${page}?${parameter}=${encodeURIComponent(value)}`;
}


function sendRequest(url) {
    return new Promise(resolve => {
        const id = guid();

        // create hidden iframe
        let iframe = document.createElement('iframe');
        iframe.id = `iframe-${id}`;
        iframe.name = `iframe-request-${id}`;
        iframe.style.visibility = 'hidden';
        iframe.style.display = 'none';

        // navigate to url
        iframe.src = url;

        // wait for onload event
        iframe.addEventListener('load', (event) => {
            document.body.removeChild(iframe);
            resolve(event);
        }, { once: true });

        // insert in existing DOM to execute
        document.body.appendChild(iframe);
    });
}

function createTimeout(duration = 1000) {
    return new Promise(resolve => setTimeout(() => resolve(`Timeout exceeded. Duration = ${duration}`), duration));
}


async function sendAction(tileName, page, actionName, callback, timeout = 5000) {
    try {

        // create url based on params
        const src = buildUrl(tileName, page, 'action', actionName)

        const requestPromise = sendRequest(src)
        const timeoutPromise = createTimeout(timeout)

        // waiting for result from request or timeout will be triggered
        return await Promise.race([ requestPromise, timeoutPromise ])

    } catch(err) {
        return err
    }
}

export {sendAction}