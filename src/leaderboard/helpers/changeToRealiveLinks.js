import React from 'react';

//  return object User: { userId, userAvatarId: string, isJiveUser: boolean } ||  { userLogoLink, isJiveUser }
const formateUserLogoLinkForSave = (userLogoLink) => {
	if (userLogoLink.includes('/api/core/v3/people/')) {
		const userId = userLogoLink.split('/people/')[1].split('/avatar')[0];
		const userAvatarId = userLogoLink.split('?a=')[1];
		const isJiveUser = true;
		return { userId, userAvatarId, isJiveUser };
	} else {
		const isJiveUser = false;
		return { userLogoLink, isJiveUser };
	}
};

export const formateUserLogoLinkForShow = (userLogoLinkData, isConfigPanel) => {
	if (typeof userLogoLinkData === 'object') {
		const { isJiveUser, ...restData } = userLogoLinkData;
		if (isJiveUser) {
			const { userId, userAvatarId } = restData;
			let formatedUserLogoLink;
			const url = `/api/core/v3/people/${userId}/avatar?a=${userAvatarId}`;
			if (isConfigPanel) {
				const origin = window.location.origin;
				formatedUserLogoLink = `${origin}${url}`;
			} else {
				formatedUserLogoLink = url;
			}
			return formatedUserLogoLink;
		} else {
			const { userLogoLink } = restData;
			return userLogoLink;
		}
	} else {
		return userLogoLinkData;
	}
};

//  return object User: { userProfileLink: string, isJiveUser: boolean }
const formateUserProfileLinkForSave = (userProfileLink) => {
	if (userProfileLink.includes('/people/')) {
		const userName = userProfileLink.split('/people/')[1];
		const isJiveUser = true;
		return { userProfileLink: userName, isJiveUser };
	} else {
		const isJiveUser = false;
		return { userProfileLink, isJiveUser };
	}
};

//recieve object User: { userName: string, isJiveUser: boolean }
export const formateUserProfileLinkForShow = (userProfileLinkData, isConfigPanel) => {
	if (typeof userProfileLinkData === 'object') {
		const { userProfileLink, isJiveUser } = userProfileLinkData;
		if (isJiveUser) {
			let formatedUserProfileLink;
			const url = `/people/${userProfileLink}`;
			if (isConfigPanel) {
				const origin = window.location.origin;
				formatedUserProfileLink = `${origin}${url}`;
			} else {
				formatedUserProfileLink = url;
			}
			return formatedUserProfileLink;
		} else {
			return userProfileLink;
		}
	} else {
		return userProfileLinkData;
	}
};

export const formateUserBeforeSaving = (boards) => {
	return boards.map((board) => {
		return {
			...board,
			users: board.users.map((user) => {
				const { userSelfLink, userLogo, userProfileLink, ...props } = user;
				return {
					...props,
					userLogo: formateUserLogoLinkForSave(userLogo),
					userProfileLink: formateUserProfileLinkForSave(userProfileLink),
				};
			}),
		};
	});
};

export const formateUserForConfigurate = (boards, isConfigPanel) => {
	return boards.map((board) => {
		return {
			...board,
			users: board.users.map((user) => {
				const { userSelfLink, userLogo, userProfileLink, ...props } = user;
				return {
					...props,
					userLogo: formateUserLogoLinkForShow(userLogo, isConfigPanel),
					userProfileLink: formateUserProfileLinkForShow(userProfileLink, isConfigPanel),
				};
			}),
		};
	});
};
