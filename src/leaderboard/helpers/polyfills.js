import "isomorphic-fetch"

// this is needed for anrom tools, because it uses "includes", but since we're using
// 'useBuiltIns: "usage"' and 'exclude: /node_modules/', in babel/webpack configs - it doesn't
// convert "includes" for IE. When we'll deal with this inconvenience in anrom tools - this can be
// removed
import "babel-runtime/core-js/array/includes"
[].includes()