import React, { useState, useEffect } from 'react';

export const Modal = ({ onRemove, children, showHover = false }) => {
  const [isOpen, setOpen] = useState(false);
  const toogleOpen = () => setOpen(prevState => !prevState);

  useEffect(() => {
    if (isOpen && showHover) {
      document.querySelector('#hover').style.display = 'block';
    }
    if (!isOpen && showHover) {
      document.querySelector('#hover').style.display = 'none';
    }
  }, [isOpen]);

  return (
    <div style={{ display: 'inline-block' }}>
      <div onClick={toogleOpen}>{children}</div>
      {isOpen && (
        <div className='modalPopover'>
          <div>Вы точно хотите удалить?</div>
          <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
            <div
              className='modalPopoverButton'
              onClick={() => {
                onRemove();
                if (showHover) {
                  document.querySelector('#hover').style.display = 'none';
                }
              }}
            >
              да
            </div>
            <div className='modalPopoverButton' onClick={toogleOpen}>
              отменить
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
