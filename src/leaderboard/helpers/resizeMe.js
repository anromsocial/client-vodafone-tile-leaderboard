import gadgets from "jive/gadgets"

export default function resizeMe() {
    gadgets.window.adjustHeight()

    /*setTimeout(() => {
        gadgets.window.adjustHeight();
    });

    setTimeout(() => {
        gadgets.window.adjustHeight();
    }, 250);*/

    setTimeout(() => {
        gadgets.window.adjustHeight()
    }, 500)

    /*setTimeout(() => {
        gadgets.window.adjustHeight();
    }, 2000);*/
}