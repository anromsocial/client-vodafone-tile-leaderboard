const DEFAULT_CONFIG = {
  boards: [],
  notifiedUsers: [],
  customHeaderTile: 'лидерборды',
  isShowImageInCollapseMode: false,
};

export { DEFAULT_CONFIG };

export const INITIAL_BOARD = {
  isNewBoard: true,
  users: [],
  // HEADER BLOCK
  title: 'новый блок',
  subtitle: '',
  imgSrc: '',
  isShowOnCollapseMode: false,
  backgroundColorHeader: {
    color: '#E72722',
    alpha: 100,
  },
  textColorHeader: {
    color: '#000000',
    alpha: 100,
  },

  // RAITING BLOCK
  backgroundColorRaitingBlock: {
    color: '#f2f8f8',
    alpha: 100,
  },
  userQuantity: 21,
  uploadUserQuantity: 3,

  // USER BLOCK
  backgroundColorUserBlock: {
    color: '#ffffff',
    alpha: 100,
  },
  textColorUserBlock: {
    color: '#000000',
    alpha: 100,
  },

  isUserNumber: false,
};

export const INITIAL_USER = {
  userName: '',
  userPosition: '',
  userProfileLink: '',
  userLogo: '',
  isNewUser: true,
  isSendedNotification: false,
};