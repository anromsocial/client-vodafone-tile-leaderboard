/**
 * This file can be overwritten with some scripts of generator-jive-react-addon. Please don't edit
 * it if you're planning to add Hot Module Replacement later
 */

import React from 'react'
import ReactDOM from 'react-dom'

export default function render(Component, config) {
    ReactDOM.render(<Component config={config}/>, document.querySelector('#main'))
}
