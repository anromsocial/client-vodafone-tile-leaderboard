import React from 'react';
import moment from 'moment';
import { Button, Intent } from '@blueprintjs/core';

const SendNotificationButton = ({ boards, onNotifyUsers, notifiedUsers }) => {
  const usersForNotify = [];

  const usersFromBoards = boards.map(board => board.users);

  usersFromBoards.forEach(usersArr =>
    usersArr.forEach(({ userId, userFromJive }) => {
      if (usersForNotify.find(user => user === userId)) return;
      if (!userFromJive) return;
      if (notifiedUsers.find(user => user.userId === userId && moment().diff(user.timeSendedNotificayion, 'hours') < 24)) return;
      usersForNotify.push(userId);
    })
  );

  return (
    <div style={{ marginBottom: 15 }}>
      {usersForNotify.length !== 0 ? (
        <Button
          style={{ marginTop: 0 }}
          className='save btn buttonSuccess'
          intent={Intent.PRIMARY}
          text='Отправить сообщение лидерам'
          onClick={() => onNotifyUsers(usersForNotify)}
        />
      ) : (
        <div>Нет сообщений для отправки</div>
      )}
    </div>
  );
};

export default SendNotificationButton;
