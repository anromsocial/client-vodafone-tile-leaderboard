import React, { useEffect } from 'react';
import { Button, Intent } from '@blueprintjs/core';
import { promiseRestGet } from 'anrom-jive-app-tools/fetchPromise';
import resizeMe from '../../../helpers/resizeMe';

const PlacePicker = ({ onChange, isMaxReachedUsers }) => {
  const onAddPeople = async people => {
    const urlForDepartment = people.resources.self.ref;
    const response = await promiseRestGet(urlForDepartment);
    let userPosition;
    try {
      const profile = response.jive.profile;
      const department = profile.filter(el => el.jive_label === 'Department')[0].value;
      userPosition = department;
    } catch (error) {
      console.log('error', error);
    }
    const fieldsForAutoComplete = {
      userId: people.id,
      userName: people.displayName,
      userPosition,
      userProfileLink: people.resources.html.ref,
      userLogo: people.resources.avatar.ref,
      userFromJive: true,
    };
    onChange(fieldsForAutoComplete);
  };

  const callJivePicker = () => {
    if(!isMaxReachedUsers) {
      const WARNING = document.querySelector('.HINT_maxUsersReach');
      WARNING.style.display = 'block';
      setTimeout(() => (WARNING.style.display = 'none'), 2000);
      return;
    }
    window.osapi.jive.corev3.search.requestPicker({
      excludeContent: true,
      excludePlaces: true,
      excludePeople: false,
      success: people => {
        onAddPeople(people);
      },
      error: err => {
        console.error('error: ' + JSON.stringify(err));
      },
    });
  };

  useEffect(() => resizeMe());

  return <Button className='save btn buttonSuccess' intent={Intent.PRIMARY} text='Найти пользователя' onClick={callJivePicker} />;
};

export default PlacePicker;
