import React from 'react';

import './ChekboxIsHiddenImage.css';

const CheckboxIsHiddenImage = ({ defaultValue, onChange }) => (
  <div className='configItemBlock headerBoardBlock checkBoxHideImg'>
    <div>Отображать изображение в свернутом виде</div>
    <input type='checkbox' name='isShowImageInCollapseMode' defaultChecked={defaultValue} onChange={onChange} />
  </div>
);

export default CheckboxIsHiddenImage;
