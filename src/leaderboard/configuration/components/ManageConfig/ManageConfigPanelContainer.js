import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown, faAngleRight } from '@fortawesome/free-solid-svg-icons';

import ManageConfig from './ManageConfig';

import resizeMe from '../../../helpers/resizeMe';

import './ManageConfigPanelContainer.css';

const ManageConfigPanel = ({ currentConfig, loadConfig }) => {
  const [isOpen, setOpen] = useState(false);
  const toogleOpen = () => setOpen(prevState => !prevState);

  const onLoadConfig = jsonText => {
    loadConfig(jsonText);
    isOpen && toogleOpen();
  };

  useEffect(() => resizeMe(), [isOpen]);
  return (
    <div className='configItemBlock'>
      <div onClick={toogleOpen}>
        <div className='headerBoardBlock'>скопировать конфигурацию</div>
        <FontAwesomeIcon className='iconArrow' size='lg' icon={isOpen ? faAngleDown : faAngleRight} />
      </div>
      {isOpen && (
        <div className='manageConfigPanel'>
          <div className='insertCopyConfig'>Вставить/скопировать конфигурацию</div>
          <ManageConfig currentConfig={currentConfig} onLoad={onLoadConfig} />
        </div>
      )}
    </div>
  );
};

export default ManageConfigPanel;
