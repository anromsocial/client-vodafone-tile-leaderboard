import React, { Component } from 'react';
import Clipboard from 'clipboard';

import './Tabs.css';
import css from './ManageConfig.css';

import { Tabs, Tab, TextArea, Button } from '@blueprintjs/core';
import resizeMe from '../../../helpers/resizeMe';

export default class ManageConfig extends Component {
  state = {
    activeTab: 'load',
    configToLoad: '',
    copied: false,
  };

  componentDidMount() {
    new Clipboard(this._copy);
  }

  render() {
    const { activeTab, configToLoad } = this.state;
    const { currentConfig } = this.props;

    const configJSON = JSON.stringify(currentConfig);

    return (
      <div className={css.main}>
        <Tabs id='configTabs' onChange={tab => this.setState({ activeTab: tab })}>
          <Tab id='load' title='Загрузить' />
          <Tab id='save' title='Сохранить' />
        </Tabs>
        <div className='tabs-content'>
          <div style={{ display: activeTab === 'load' ? 'block' : 'none' }}>
            <TextArea
              value={configToLoad}
              onChange={e => this.setState({ configToLoad: e.target.value })}
              fill={true}
              placeholder='Вставьте конфигурацию и нажмите "Загрузить конфигурацию"'
            />
            <Button className='action-button' text='Загрузить конфигурацию' onClick={::this.loadConfig} />
          </div>

          <div style={{ display: activeTab === 'save' ? 'block' : 'none' }}>
            <TextArea readOnly value={configJSON} fill={true} />
            <button
              className='bp3-button action-button'
              data-clipboard-text={configJSON}
              ref={el => {
                if (!this._copy) this._copy = el;
              }}
              onClick={() => {
                this.setState({ copied: true }), resizeMe();
              }}
            >
              Скопировать в буфер обмена
            </button>
          </div>
          {this.state.copied && <div className='copy-to-buffer'>Текст скопирован в буфер обмена</div>}
        </div>
      </div>
    );
  }

  loadConfig() {
    try {
      this.props.onLoad(JSON.parse(this.state.configToLoad));
    } catch (error) {
      console.error('probably malformed JSON');
    }

    this.setState({
      configToLoad: '',
    });
  }
}
