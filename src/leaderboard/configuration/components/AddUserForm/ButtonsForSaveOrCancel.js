import React from 'react';
import { Button, Intent } from '@blueprintjs/core';

const ButtonsForSaveOrCancel = ({ user, createOrUpdateUser, isOpenAddOrEditUserForm, onCancel }) => (
  <div style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'flex-end', marginTop: '-20px', marginBottom: 30 }}>
    {user.userName !== '' && (
      <div style={{ marginRight: 10 }}>
        <Button className='save btn buttonSuccess' intent={Intent.PRIMARY} text={user.isNewUser ? 'добавить пользователя' : 'сохранить пользователя'} onClick={() => createOrUpdateUser(user)} />
      </div>
    )}

    {isOpenAddOrEditUserForm && <Button className='cancel btn buttonDefault' text='Отмена' onClick={onCancel} />}
  </div>
);

export default ButtonsForSaveOrCancel;
