import React, { memo } from 'react';

const UserForm = ({ userName, userPosition, userProfileLink, userLogo, onHandleChangeUser }) => (
  <div className='userForm'>
    <div className='inputBlock'>
      <div className='typeConfigTitle'>фио</div>
      <input type='text' name='userName' value={userName} placeholder='вставьте текст' onChange={onHandleChangeUser} autoComplete='off' required />
    </div>

    <div className='inputBlock'>
      <div className='typeConfigTitle'>подразделение <div className='optionality'>(опционально)</div></div>
      <input type='text' name='userPosition' value={userPosition} placeholder='вставьте текст' onChange={onHandleChangeUser} autoComplete='off' />
    </div>

    <div className='inputBlock'>
      <div className='typeConfigTitle'>
        ссылка <div className='optionality'>(опционально)</div>
      </div>
      <input type='text' name='userProfileLink' value={userProfileLink} placeholder='вставьте ссылку' onChange={onHandleChangeUser} autoComplete='off' />
    </div>

    <div className='inputBlock'>
      <div className='typeConfigTitle'>
        картинка <div className='optionality'>(опционально)</div>
      </div>
      <input type='text' name='userLogo' value={userLogo} placeholder='вставьте картинку' onChange={onHandleChangeUser} autoComplete='off' />
    </div>
  </div>
);
export default memo(UserForm);
