import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown, faAngleRight } from '@fortawesome/free-solid-svg-icons';

import './HeaderLeaders.css'

const HeaderLeaders = ({ isOpen, onClick }) => (
  <div className='headerLeadersListBlock' onClick={onClick}>
    <div className='typeConfigTitle' style={{ cursor: 'pointer' }}>лучшие сотрудники/команды</div>
    <FontAwesomeIcon className='iconArrow' size='lg' icon={isOpen ? faAngleDown : faAngleRight} />
  </div>
);

export default HeaderLeaders;