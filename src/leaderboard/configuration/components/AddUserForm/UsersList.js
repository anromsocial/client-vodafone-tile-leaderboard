import React, { useState } from 'react';
import { SortableContainer, SortableElement } from 'react-sortable-hoc';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt, faPencilAlt, faEnvelope, faCaretUp, faCaretDown } from '@fortawesome/free-solid-svg-icons';

import { Modal } from '../../../helpers/Popover';

const UsersList = ({ users, onSortEnd, editUser, removeUser, isShowUserNumber, onChangeUserNumber }) => {
  const SortableItem = SortableElement(({ user, userNumber }) => {
    const [value, setValue] = useState(user.userNumber || userNumber + 1);
    const changeUserNumber = ({ target }) => {
      const number = Number(target.value);
      if (!number) return;
      if (number < 1 || number > 99) return;
      setValue(number);
    };

    const increment = () => {
      const number = value + 1;
      if (number > 99) return;
      setValue(number);
    };

    const decrement = () => {
      const number = value - 1;
      if (number < 1) return;
      setValue(number);
    };

    const onBlurFunction = userId => {
      const scrollPosition = document.querySelector('.boardListBlock').scrollTop;
      onChangeUserNumber(userId, value, scrollPosition);
    };

    return (
      <div className='boardListItem'>
        <div className='boardListItemTitle'>
          {isShowUserNumber && (
            <>
              <input
                type='number'
                className='userNumberInput'
                name={user.userId}
                value={value}
                onChange={changeUserNumber}
                onBlur={() => onBlurFunction(user.userId)}
              />
              <div className='inputArrows' onMouseLeave={() => onBlurFunction(user.userId)}>
                <FontAwesomeIcon className='inputIncrement' icon={faCaretUp} onClick={increment} />
                <FontAwesomeIcon className='inputDecrement' icon={faCaretDown} onClick={decrement} />
              </div>
            </>
          )}{' '}
          {user.userName}{' '}
          {user.userFromJive && (
            <FontAwesomeIcon
              style={{ color: user.isSendedNotification ? 'green' : 'black', cursor: 'pointer', marginLeft: 5, minWidth: '1em' }}
              title={`Уведомление ${!user.isSendedNotification ? 'не ' : ''}отправлено`}
              icon={faEnvelope}
            />
          )}
        </div>
        <div className='boardListItemTools'>
          <FontAwesomeIcon
            title='редактировать'
            className='editButton2'
            style={{ cursor: 'pointer', marginRight: '10px' }}
            icon={faPencilAlt}
            onClick={() => window.parent.scrollTo(0, 1340) || editUser(user.userId)}
          />
          <Modal onRemove={() => removeUser(user.userId)}>
            <FontAwesomeIcon title='удалить' className='deleteButton2' style={{ cursor: 'pointer' }} icon={faTrashAlt} />
          </Modal>
        </div>
      </div>
    );
  });

  const SortableBoardList = SortableContainer(({ items }) => (
    <div className='boardListBlock'>
      {items.map((item, index) => (
        <SortableItem key={item.userId} index={index} userNumber={index} user={item} />
      ))}
    </div>
  ));

  return users.length > 0 && <SortableBoardList items={users} onSortEnd={onSortEnd} lockAxis='y' distance={20} />;
};

export default UsersList;
