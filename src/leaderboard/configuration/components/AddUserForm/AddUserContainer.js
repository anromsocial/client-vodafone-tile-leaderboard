import React, { useState, useEffect, useLayoutEffect } from 'react';

import HeaderLeaders from './HeaderLeaders';
import ButtonsForAddUser from './ButtonsForAddUser';
import PlacePicker from '../PlacePicker/PlacePicker';
import UserForm from './UserForm';
import ButtonsForSaveOrCancel from './ButtonsForSaveOrCancel';
import UsersList from './UsersList';

import resizeMe from '../../../helpers/resizeMe';
import { INITIAL_USER } from '../../../constants/configDefaults';

import './AddUserContainer.css';

const AddUser = ({ onHandleCreateOrUpdateUser, users, onSortEnd, onRemoveUser, isShowUserNumber }) => {
  const [isOpen, setOpen] = useState(false);
  const toogleOpen = () => setOpen(prevState => !prevState);

  const [isOpenAddOrEditUserForm, setAddOrEditUserForm] = useState(false);
  const toogleOpenAddOrEditUserForm = () => setAddOrEditUserForm(prevState => !prevState);

  const onHandleOpenFormForAddUserManually = () => {
    if(users.length < 99) {
      !isOpenAddOrEditUserForm && toogleOpenAddOrEditUserForm();
      return
    };
    const WARNING = document.querySelector('.HINT_maxUsersReach');
    WARNING.style.display = 'block';
    setTimeout(() => WARNING.style.display = 'none', 2000)
  };

  const [user, setUser] = useState(INITIAL_USER);
  const onHandleChangeUser = ({ target }) => {
    const value = target.type === 'checkbox' ? target.checked : target.value;
    setUser(prevState => ({ ...prevState, [target.name]: value }));
  };

  const [scrollPosition, setScrollPosition] = useState(0);
  const changeScrollPositon = scrollTop => setScrollPosition(scrollTop);

  const [isChange, setIsChange] = useState(false);

  const onHandleSetUserFromPicker = user => {
    if (users.find(({ userId }) => userId === user.userId)) return;
    !isOpenAddOrEditUserForm && toogleOpenAddOrEditUserForm();
    setUser(prevState => ({ ...prevState, ...user }));
  };

  const createOrUpdateUser = user => {
    if (user.userName === '') return;
    onHandleCreateOrUpdateUser(user);
    setUser(INITIAL_USER);
    const userList = document.querySelector('.addUserLeaderBlock .boardListBlock');
    if(userList) {
      setTimeout(() => {
        const getNewUserList = document.querySelector('.addUserLeaderBlock .boardListBlock');
        const userListScrollHeight = getNewUserList.scrollHeight;
        if (getNewUserList.offsetHeight < userListScrollHeight) {
          getNewUserList.scrollTop = getNewUserList.offsetHeight;
        };
      }, 100);
    }
    toogleOpenAddOrEditUserForm();
  };

  const onChangeUserNumber = (userId, number, scrollTop) => {
    const userForUpdate = users.find(user => user.userId === userId);
    onHandleCreateOrUpdateUser({ ...userForUpdate, userNumber: number });
    changeScrollPositon(scrollTop);
    setIsChange(true);
  };

  const editUser = userId => {
    !isOpenAddOrEditUserForm && toogleOpenAddOrEditUserForm();
    setUser(users.find(user => user.userId === userId));
  };

  const removeUser = userId => {
    if (isOpenAddOrEditUserForm && users.find(({ userId }) => user.userId === userId)) {
      setUser(INITIAL_USER);
      toogleOpenAddOrEditUserForm();
    }
    onRemoveUser(userId);
  };

  const onCancel = () => {
    setUser(INITIAL_USER);
    toogleOpenAddOrEditUserForm();
  };

  const sortUser = ({ oldIndex, newIndex }) => {
    const scrollPosition = document.querySelector('.addUserLeaderBlock .boardListBlock').scrollTop;
    changeScrollPositon(scrollPosition);
    setIsChange(true);
    onSortEnd({ oldIndex, newIndex });
  };

  useEffect(() => {
    setIsChange(false);
    if (!isChange) {
      const userList = document.querySelector('.addUserLeaderBlock .boardListBlock');
      if (!!userList) {
        userList.scrollTop = scrollPosition;
      }
    }
  }, [isChange]);

  useEffect(() => resizeMe(), [isOpen]);

  return (
    <div className='addUserLeaderBlock'>
      <HeaderLeaders isOpen={isOpen} onClick={toogleOpen} />
      {isOpen && (
        <>
          <ButtonsForAddUser onClick={onHandleOpenFormForAddUserManually}>
            <PlacePicker onChange={onHandleSetUserFromPicker} isMaxReachedUsers={users.length < 99} />
          </ButtonsForAddUser>

          {isOpenAddOrEditUserForm && <UserForm {...user} onHandleChangeUser={onHandleChangeUser} />}

          <ButtonsForSaveOrCancel
            user={user}
            createOrUpdateUser={createOrUpdateUser}
            isOpenAddOrEditUserForm={isOpenAddOrEditUserForm}
            onCancel={onCancel}
          />

          <UsersList
            users={users}
            onSortEnd={sortUser}
            editUser={editUser}
            removeUser={removeUser}
            isShowUserNumber={isShowUserNumber}
            onChangeUserNumber={onChangeUserNumber}
          />
        </>
      )}
    </div>
  );
};

export default AddUser;
