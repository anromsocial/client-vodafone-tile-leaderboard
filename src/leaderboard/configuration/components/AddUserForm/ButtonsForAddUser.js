import React from 'react';
import { Button, Intent } from '@blueprintjs/core';

import './ButtonsForAddUser.css';

const ButtonsForAddUser = ({ children, onClick }) => (
  <div className='leadersBlock'>
    <div className='listLeadersTitle'>список лидеров</div>

    <div className='buttonsBlockForFindUsers'>
      {children}
      <div>или</div>
      <Button className='save btn buttonSuccess' intent={Intent.PRIMARY} text='Добавить вручную' onClick={onClick} />
    </div>

    <div className='HINT_maxUsersReach'>Максимальное количество пользователей 99!</div>
  </div>
);

export default ButtonsForAddUser;
