import React, { useState } from 'react';

import './CustomHeaderTile.css';

const CustomHeaderTile = ({ onChange, customHeader }) => {
  const [isChange, setIsChange] = useState(false);
  const toogleIsChange = () => setIsChange(prevState => !prevState);
  return (
    <div className='customHeaderBlock'>
      {isChange ? (
        <input className='input' type='text' name='customHeaderText' value={customHeader} onChange={onChange} onBlur={toogleIsChange} autoComplete='off' />
      ) : (
        <>
          <div className='customHeader' onDoubleClick={toogleIsChange}>
            {customHeader}
          </div>
          <div className='customHeaderHint'>кликнете дважды на заголовок для редактирования (опционально)</div>
        </>
      )}
    </div>
  );
};

export default CustomHeaderTile;
