import jive from 'jive';
import React, { useState, useEffect } from 'react';
import { promiseRestPost } from 'anrom-jive-app-tools/fetchPromise';
import arrayMove from '../../../helpers/array-move';
import moment from 'moment';

import HeaderConfigPanel from '../HeaderConfigPanel/HeaderConfigPanel';
import CustomHeaderTile from '../CustomHeaderTile/CustomHeaderTile';
import AddBoard from '../AddBoardForm/AddBoardContainer';
import CheckboxIsHiddenImage from '../Checkbox/ChekboxIsHiddenImage';
import BoardsList from '../BoardsList/BoardsList';
import ManageConfigPanel from '../ManageConfig/ManageConfigPanelContainer';
import SendNotificationButton from '../SendNotification/SendNotificationButton';
import ButtonsForSaveOrCancelTileConfigurations from './ButtonsForSaveOrCancelTileConfiguration';

import { ToasterSuccess, ToasterWarning, ToasterError } from '../../../helpers/Toasts';
import { getPageUrl } from '../../../helpers/getUrlPage';

import reload from '../../../helpers/reload';
import { prepareConfig } from '../../../helpers/dataUtils';
import FontsLoader from '../../../helpers/FontsLoader';
import { uId } from '../../../helpers/idGenerator';
import { sendAction } from '../../../helpers/DomActions';
import css from './TileConfiguration.scss';

import { DEFAULT_CONFIG, INITIAL_BOARD } from '../../../constants/configDefaults';
import { CONVERTERS } from '../../../constants/converters';
import { formateUserBeforeSaving, formateUserForConfigurate } from '../../../helpers/changeToRealiveLinks';

const TileConfiguration = ({ config }) => {
  const [tileConfig, setTileConfig] = useState(config);

  const [notifiedUsers, setNotifiedUsers] = useState(config.notifiedUsers);

  const [boards, setBoards] = useState(config.boards);

  const [board, setBoardProp] = useState(INITIAL_BOARD);
  const onHandleCreateBoard = board => {
    const id = uId(boards);
    setBoards(prevState => [...prevState, { ...board, id }]);
    setBoardProp(INITIAL_BOARD);
    const boardList = document.querySelector('.headerBoardListBlock + .boardListBlock');
    if (boardList) {
      setTimeout(() => {
        const getNewBoardList = document.querySelector('.headerBoardListBlock + .boardListBlock');
        const boardListScrollHeight = getNewBoardList.scrollHeight;
        if (getNewBoardList.offsetHeight < boardListScrollHeight) {
          getNewBoardList.scrollTop = boardListScrollHeight;
        }
      }, 300);
    }
    window.parent.scrollTo(0, 500);
  };

  const onHandleChooseBoardForUpdate = id => setBoardProp({ ...boards.find(board => board.id === id), isEditBoard: true });

  const onHandleUpdateBoard = board => {
    const getBoardForUpdate = boards.find(el => el.id === board.id);
    const getIdxBoardForUpdate = boards.indexOf(getBoardForUpdate);
    const updatedBoard = [...boards.slice(0, getIdxBoardForUpdate), board, ...boards.slice(getIdxBoardForUpdate + 1)];
    setBoards(updatedBoard);
    setBoardProp(INITIAL_BOARD);
    window.parent.scrollTo(0, 500);
  };

  const checkBoardsLimit = () => {
    if(boards.length === 10) {
      ToasterWarning.show({ message: 'Максимальное количество блоков - 10' });
      return false;
    }
    return true;
  }

  const onCancelBoard = () => setBoardProp(INITIAL_BOARD) || window.parent.scrollTo(0, 500);

  const onHandleRemoveBoard = id => {
    board.id === id && setBoardProp(INITIAL_BOARD);
    setBoards(prevState => prevState.filter(board => board.id !== id));
    setBoardProp(INITIAL_BOARD);
    window.parent.scrollTo(0, 500);
  };

  const [customHeaderTile, setCustomHeader] = useState(config.customHeaderTile);
  const changeCustomeHeader = ({ target }) => setCustomHeader(target.value);

  const onSortEnd = ({ oldIndex, newIndex }) => setBoards(prevState => arrayMove(prevState, oldIndex, newIndex));

  const [isShowImageInCollapseMode, setShowImageInCollapseMode] = useState(config.isShowImageInCollapseMode);
  const onHandleChangeShowImageInCollapseMode = () => setShowImageInCollapseMode(prevState => !prevState);

  const onNotifyUsers = async usersForNotify => {
    console.log('usersForNotify', usersForNotify);

    const participants = usersForNotify.map(userId => `/people/${userId}`);

    const { resources } = await getPageUrl();

    const pageBoardUrl = resources.html.ref;

    const response = await promiseRestPost('/api/core/v3/dms', {
      type: 'application/json',
      body: JSON.stringify({
        content: {
          type: 'text/html',
          text: `<body><p>Переглянь деталі <a href='${pageBoardUrl}' target='_blank'>тут</a></p></body>`,
        },
        subject: 'Вітаємо! Ти в рейтингу найкращих!',
        participants,
      }),
    });

    console.log('response after send notification for users', response);

    if (response.status === 'published' || response.status === 201) {
      ToasterSuccess.show({ message: 'Сообщения отправлены' }) &&
        setBoards(
          boards.map(board => ({
            ...board,
            users: board.users.map(user => {
              const isSendedNotification = usersForNotify.includes(user.userId);
              return user.userFromJive && !user.isSendedNotification ? { ...user, isSendedNotification } : user;
            }),
          }))
        );
      setNotifiedUsers(prevState => {
        const usersWasNotified = [];
        const usersFromBoards = boards.map(board => board.users);
        usersFromBoards.forEach(usersArr =>
          usersArr.forEach(
            user => console.log('user forEach',user) ||
              user.userFromJive &&
              !user.isSendedNotification &&
              !prevState.find(({ userId }) => user.userId === userId) &&
              !usersWasNotified.find(({ userId }) => userId === user.userId) &&
              usersWasNotified.push({ userId: user.userId, timeSendedNotificayion: moment() })
          )
        );

        const updateTimeNotificayion = prevState.map( user => {
          if(usersForNotify.includes(user.userId)) {
            return { ...user, timeSendedNotificayion: moment() }
          }
          return user;
        })

        return [...updateTimeNotificayion, ...usersWasNotified];
      });
    } else {
      ToasterError.show({ message: 'Сообщения не отправлены' });
    }
  };

  const loadConfig = async jsonText => {
    try {
      let newConfig;

      try {
        JSON.stringify(jsonText);
        newConfig = jsonText;
      } catch (jsonErr) {
        console.error('new config JSON is not valid');
        return;
      }

      const preparedConfig = await prepareConfig(newConfig, DEFAULT_CONFIG, CONVERTERS);

      setTileConfig(preparedConfig);
    } catch (error) {
      console.error(error);
    }
  };

  const cancelSettings = async () => {
    await sendAction(process.env.APP_NAME, 'dom_actions.html', 'enable_popup_closing');

    jive.tile.close();
  };

  const saveSettings = async () => {
    await sendAction(process.env.APP_NAME, 'dom_actions.html', 'enable_popup_closing');
    const foramtedUsersFieldsInBoards = formateUserBeforeSaving(boards);
    jive.tile.close({ data: { boards: foramtedUsersFieldsInBoards, notifiedUsers, customHeaderTile, isShowImageInCollapseMode } }, {});
  };

  useEffect(() => console.log('TIME', notifiedUsers), [notifiedUsers]);

  useEffect(() => {
    const formatedUserFieldsForBoards = formateUserForConfigurate(tileConfig.boards, true);
    setBoards(formatedUserFieldsForBoards);
    setNotifiedUsers(tileConfig.notifiedUsers);
    setCustomHeader(tileConfig.customHeaderTile);
    setShowImageInCollapseMode(tileConfig.isShowImageInCollapseMode);
  }, [tileConfig]);

  return (
    <div className={css.main} onMouseUp={reload}>
      <FontsLoader />

      <HeaderConfigPanel />

      <CustomHeaderTile onChange={changeCustomeHeader} customHeader={customHeaderTile} />

      <AddBoard
        onHandleCreateBoard={onHandleCreateBoard}
        updateBoard={onHandleUpdateBoard}
        onCancelBoard={onCancelBoard}
        onRemoveBoard={onHandleRemoveBoard}
        boardProp={board}
        checkBoardsLimit={checkBoardsLimit}
      />

      <BoardsList boards={boards} onSortEnd={onSortEnd} editBoard={onHandleChooseBoardForUpdate} onRemoveBoard={onHandleRemoveBoard} />

      <CheckboxIsHiddenImage defaultValue={isShowImageInCollapseMode} onChange={onHandleChangeShowImageInCollapseMode} />

      <SendNotificationButton boards={boards} onNotifyUsers={onNotifyUsers} notifiedUsers={notifiedUsers} />

      <ManageConfigPanel currentConfig={tileConfig} loadConfig={loadConfig} />

      <ButtonsForSaveOrCancelTileConfigurations
        isDisabled={boards.length === 0 || board.isEditBoard ? true : false}
        saveTileConfig={saveSettings}
        cancelTileConfig={cancelSettings}
      />
    </div>
  );
};

export default TileConfiguration;
