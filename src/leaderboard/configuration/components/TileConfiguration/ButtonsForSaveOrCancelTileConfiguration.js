import React from 'react';
import { Button, Intent } from '@blueprintjs/core';

import'./ButtonsForSaveOrCancelTileConfiguration.css'

const ButtonsForSaveOrCancelTileConfigurations = ({ isDisabled, saveTileConfig, cancelTileConfig }) => (
  <div className='buttonGroup'>
    <Button disabled={isDisabled} className={`save btn ${isDisabled ? 'buttonDisabled' : 'buttonSuccess'}`} intent={Intent.PRIMARY} text='Сохранить' onClick={saveTileConfig} />
    <Button className='cancel btn buttonDefault' text='Отмена' onClick={cancelTileConfig} />
  </div>
);

export default ButtonsForSaveOrCancelTileConfigurations;
