import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown, faAngleRight } from '@fortawesome/free-solid-svg-icons';

import './HeaderCreateOrEditBlock.css';

const HeaderCreateOrEditBlock = ({ isOpen, onOpen, isNewBoard, boardTitle }) => (
  <div className='boardBlock' onClick={onOpen}>
    {isNewBoard ? (
      <div className='addBlockButton'>добавить блок</div>
    ) : (
      <>
        <div className='headerBoardBlock'>{boardTitle}</div>
        <FontAwesomeIcon className='iconArrow' size='lg' icon={isOpen ? faAngleDown : faAngleRight} />
      </>
    )}
  </div>
);

export default HeaderCreateOrEditBlock;
