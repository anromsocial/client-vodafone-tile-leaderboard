import React, { useState, useEffect } from 'react';
import uuid from 'uuid';
import arrayMove from '../../../helpers/array-move';

import HeaderCreateOrEditBlock from './HeaderCreateOrEditBlock';
import FormBoard from './FormBoard';
import AddUser from '../AddUserForm/AddUserContainer';
import FooterButtonBoardGroup from './FooterButtonBoardGroup';

import resizeMe from '../../../helpers/resizeMe';
import { INITIAL_BOARD } from '../../../constants/configDefaults';


import './AddBoardContainer.css';

const AddBoard = ({ onHandleCreateBoard, boardProp, updateBoard, onCancelBoard, onRemoveBoard, checkBoardsLimit }) => {
  const [isOpen, setOpen] = useState(false);
  const toogleOpen = () => setOpen(prevState => !prevState);

  const [board, setBoard] = useState(boardProp);

  const onHandleChangeBoard = ({ target }) => {
    const value = target.type === 'checkbox' ? target.checked : target.value;
    setBoard(prevState => ({ ...prevState, [target.name]: value }));
  };

  const onNumberChange = (name, value) => setBoard(prevState => ({ ...prevState, [name]: value }));

  const [users, setUser] = useState(boardProp.users);

  const onHandleCreateOrUpdateUser = user => {
    const isUserExist = users.find(({ userId }) => userId === user.userId);
    if (!!isUserExist) {
      const getUserForUpdate = users.find(el => el.userId === user.userId);
      const getIdxUserForUpdate = users.indexOf(getUserForUpdate);
      const updatedUser = [...users.slice(0, getIdxUserForUpdate), user, ...users.slice(getIdxUserForUpdate + 1)];
      if(getUserForUpdate.userNumber === user.userNumber) {
        setUser(updatedUser);
        return;
      }
      const sortedUsers = updatedUser.sort( (a, b) => a.userNumber - b.userNumber);
      setUser(sortedUsers);
      return;
    }

     const lastUser = users[users.length - 1];
     let userObj = {};
     if (users.length !== 0 && lastUser.hasOwnProperty('userNumber')) {
        userObj = {...user, userNumber: lastUser.userNumber === 99 ? 99 : lastUser.userNumber + 1}
     } else {
        userObj = user;
     }

    const newUser = user.hasOwnProperty('userId') ? { ...userObj, isNewUser: false } : { ...userObj, userId: uuid.v4(), isNewUser: false };
    setUser(prevState => [...prevState, {...newUser,
      userNumber: prevState.length ? 
        prevState[prevState.length - 1].userNumber === 99 ?
           99 :
          prevState[prevState.length - 1].userNumber + 1 :
             1
    }]);
    setTimeout(() => {
      const userList = document.querySelector('.addUserLeaderBlock .boardListBlock');
      if(userList) {
        const ListScrollHeight = userList.scrollHeight;
        if(userList.offsetHeight < ListScrollHeight) {
          userList.scrollTop = ListScrollHeight;
        }
      }
    }, 300)
  };

  const onRemoveUser = userId => setUser(prevState => {
    const userForRemove = users.find( el => el.userId === userId);
    const userIdxForRemove = users.indexOf(userForRemove);
    const nextUser = users[userIdxForRemove + 1];
    let diff = 1;
    if(!!nextUser) {
      diff = nextUser.userNumber - users[userIdxForRemove].userNumber;
    }
    const filteredUsers = users.map( (el, idx) => {
      if(idx > userIdxForRemove) {
        return {...el, userNumber: el.userNumber - diff}
      };
      return el;
    }).filter( el => el.userId !== userId);
    return filteredUsers;
  });

  const onUpdatedBoard = () => {
    updateBoard({ ...board, users });
    toogleOpen();
  };

  const changeColor = (type, color) => setBoard(prevState => ({ ...prevState, [type]: color }));

  const onSortEnd = ({ oldIndex, newIndex }) =>
    setUser(prevState => {
      const sortedUsers = arrayMove(prevState, oldIndex, newIndex);
      const getUserForUpdateNumber = sortedUsers[newIndex];

      if(oldIndex - newIndex === 1 && sortedUsers[oldIndex].userNumber === sortedUsers[newIndex].userNumber) {
        return sortedUsers;
      }

      if(newIndex - oldIndex === 1 && sortedUsers[oldIndex].userNumber === sortedUsers[newIndex].userNumber) {
        return sortedUsers;
      }

      // if(newIndex - oldIndex === 1 && sortedUsers[oldIndex].userNumber !== sortedUsers[newIndex].userNumber) {
      //   const usersBefore = sortedUsers.slice(0, oldIndex);
      //   let diff = sortedUsers[oldIndex].userNumber - sortedUsers[newIndex].userNumber;
      //   let usersForUpdate = sortedUsers.slice(oldIndex, newIndex).map( el => ({...el, userNumber: el.userNumber - diff }));
      //   let usersAfter = sortedUsers.slice(newIndex + 1);

      //   let userShouldUpdate = {...getUserForUpdateNumber, userNumber: usersForUpdate[usersForUpdate.length - 1].userNumber + diff};
      //   let updatedSortedUsers = [...usersBefore, ...usersForUpdate, userShouldUpdate, ...usersAfter]
      //   return updatedSortedUsers
      // }

      // if(oldIndex - newIndex === 1 && sortedUsers[oldIndex].userNumber !== sortedUsers[newIndex].userNumber) {
      //   let otherUsersBefore = sortedUsers.slice(0, newIndex);
      //   let different = sortedUsers[newIndex].userNumber - sortedUsers[oldIndex].userNumber;
      //   let usersForUpdates = sortedUsers.slice(oldIndex, newIndex).map( el => ({...el, userNumber: el.userNumber - different }));
      //   let otherAfterUsers = sortedUsers.slice(oldIndex + 1);

      //   let userWillUpdate = {...getUserForUpdateNumber, userNumber: usersForUpdates[usersForUpdates.length - 1].userNumber + different};
      //   let usersUpdated = [...otherUsersBefore, ...usersForUpdates, userWillUpdate, ...otherAfterUsers]
      //   return usersUpdated
      // }

      if(newIndex > oldIndex) {
        const usersBeforeUserWhichUpdated = sortedUsers.slice(0, oldIndex);
        const countDif = sortedUsers[oldIndex].userNumber - sortedUsers[newIndex].userNumber;
        const dif = countDif ? countDif : 1;
        const usersWhichUpdated = sortedUsers.slice(oldIndex, newIndex).map( el => ({...el, userNumber: el.userNumber - dif }));
        const usersAfterUserWhichUpdated = sortedUsers.slice(newIndex + 1)
        
        const userForUpdate = {...getUserForUpdateNumber, userNumber: usersWhichUpdated[usersWhichUpdated.length - 1].userNumber + dif  === 100 ? 99 : usersWhichUpdated[usersWhichUpdated.length - 1].userNumber + dif }; 

        const updatedSortedUsers = [...usersBeforeUserWhichUpdated, ...usersWhichUpdated, userForUpdate, ...usersAfterUserWhichUpdated]
        // .sort((a,b) => a.userNumber - b.userNumber);

        return updatedSortedUsers;

      }
      
      if(newIndex < oldIndex) {
        const before = sortedUsers.slice(0, newIndex);

        const after = sortedUsers.slice(oldIndex + 1);

        const calcDiff = sortedUsers[newIndex].userNumber - sortedUsers[oldIndex].userNumber;
        const diff = calcDiff ? calcDiff : 1;

        const updatedUsers = sortedUsers.slice(newIndex + 1, oldIndex + 1).map( el => ({...el, userNumber: el.userNumber + diff === 100 ? 99 : el.userNumber + diff  }) );

        const updateUser = {...getUserForUpdateNumber, userNumber: updatedUsers[0].userNumber - diff};
       

        const updatedSortedUsers = [...before, updateUser, ...updatedUsers, ...after]
        
        // .sort((a,b) => a.userNumber - b.userNumber);

        return updatedSortedUsers;
      }

      return sortedUsers;
      }
    )

  const onCreateBoard = () => {
    const newBoard = { ...board, isNewBoard: false, users };
    onHandleCreateBoard(newBoard);
    setBoard(INITIAL_BOARD);
    setUser([]);
    toogleOpen();
  };

  const onCancel = () => {
    setBoard(INITIAL_BOARD);
    setUser([]);
    onCancelBoard();
    toogleOpen();
  };

  const removeBoard = () => {
    toogleOpen();
    onRemoveBoard(board.id);
    window.parent.scrollTo(0, 500);
  };

  useEffect(() => resizeMe(), [isOpen]);

  useEffect(() => {
    boardProp.isEditBoard && !isOpen && toogleOpen();
    boardProp.isNewBoard && isOpen && toogleOpen();
    setBoard(boardProp);
    setUser(boardProp.users);
  }, [boardProp]);

  return (
    <div className='configItemBlock'>
      <HeaderCreateOrEditBlock 
        isOpen={isOpen}
        onOpen={() => checkBoardsLimit() && toogleOpen()}
        isNewBoard={board.isNewBoard}
        boardTitle={board.title} />

      {isOpen && (
        <>
          <FormBoard {...board} onHandleChangeBoard={onHandleChangeBoard} onHandleNumberChange={onNumberChange} changeColor={changeColor}>
            <AddUser
              users={users}
              isShowUserNumber={board.isUserNumber}
              onSortEnd={onSortEnd}
              onHandleCreateOrUpdateUser={onHandleCreateOrUpdateUser}
              onRemoveUser={onRemoveUser}
            />
          </FormBoard>

          <FooterButtonBoardGroup
            board={board}
            usersQuantity={users.length}
            onCreateBoard={onCreateBoard}
            onUpdatedBoard={onUpdatedBoard}
            removeBoard={removeBoard}
            onCancel={onCancel}
          />
        </>
      )}
    </div>
  );
};

export default AddBoard;
