import React from 'react';
import ColorPicker from 'rc-color-picker';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretUp, faCaretDown } from '@fortawesome/free-solid-svg-icons';

import 'rc-color-picker/assets/index.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/icons/lib/css/blueprint-icons.css';

import './FormBoard.css';

const FormBoard = ({
  title,
  subtitle,
  imgSrc,
  userQuantity,
  uploadUserQuantity,
  isUserNumber,
  backgroundColorHeader,
  textColorHeader,
  backgroundColorRaitingBlock,
  backgroundColorUserBlock,
  textColorUserBlock,
  onHandleChangeBoard,
  onHandleNumberChange,
  changeColor,
  children,
}) =>
  (
    <div className='formBoard'>
      <div className='inputBlock'>
        <div className='typeConfigTitle'>
          заголовок <div className='optionality'>(опционально)</div>
        </div>
        <input type='text' name='title' value={title} placeholder='введите название' required onChange={onHandleChangeBoard} autoComplete='off' />
        <div className='hintText'>Этот текст будет отображаться в заголовке блока.</div>
      </div>

      <div className='inputBlock'>
        <div className='typeConfigTitle'>
          подзаголовок <div className='optionality'>(опционально)</div>
        </div>
        <input type='text' name='subtitle' value={subtitle} placeholder='введите название' onChange={onHandleChangeBoard} autoComplete='off' />
        <div className='hintText'>Этот текст будет отображаться в подзаголовке блока.</div>
      </div>

      <div className='inputBlock'>
        <div className='typeConfigTitle'>
          url картинки <div className='optionality'>(опционально)</div>
        </div>
        <input type='text' name='imgSrc' value={imgSrc} placeholder='вставьте ссылку' onChange={onHandleChangeBoard} autoComplete='off' />
      </div>

      <div className='inputBlock'>
        <div className='typeConfigTitle'>количество отображаемых сотрудников до кнопки "Показать еще"</div>
        <div style={{ display: 'flex', alignItems: 'center', marginLeft: 20}}>
          <input
            type='number'
            className='userNumberInput'
            value={userQuantity}
            onChange={({ target: { value } }) => {
                const number = Number(value);
                if (!number) return;
                if (number < 1 || number > 21) return;
                onHandleNumberChange('userQuantity', number)
              }
            }
          />
          <div className='inputArrows'>
            <FontAwesomeIcon className='inputIncrement' icon={faCaretUp} onClick={() =>
              userQuantity + 1 <= 21 && onHandleNumberChange('userQuantity', userQuantity + 1)
            } />
            <FontAwesomeIcon className='inputDecrement' icon={faCaretDown} onClick={() =>
              userQuantity - 1 > 0 && onHandleNumberChange('userQuantity', userQuantity - 1)
            } />
          </div>
        </div>
      </div>

      <div className='inputBlock'>
        <div className='typeConfigTitle'>количество подгружаемых сотрудников для кнопки "Показать еще"</div>
        <div style={{ display: 'flex', alignItems: 'center', marginLeft: 20}}>
          <input
            type='number'
            className='userNumberInput'
            value={uploadUserQuantity}
            onChange={({ target: { value } }) => {
                const number = Number(value);
                if (!number) return;
                if (number < 1 || number > 21) return;
                onHandleNumberChange('uploadUserQuantity', number)
              }
            }
          />
          <div className='inputArrows'>
            <FontAwesomeIcon className='inputIncrement' icon={faCaretUp} onClick={() =>
              uploadUserQuantity + 1 <= 21 && onHandleNumberChange('uploadUserQuantity', uploadUserQuantity + 1)
            } />
            <FontAwesomeIcon className='inputDecrement' icon={faCaretDown} onClick={() =>
              uploadUserQuantity - 1 > 0 && onHandleNumberChange('uploadUserQuantity', uploadUserQuantity - 1)
            } />
          </div>
        </div>
      </div>

      <div className='inputBlock inputBlockCustomStyle'>
        <div className='typeConfigTitle customTitle'>отображать порядковый номер</div>
        <input type='checkbox' className='isUserNumber' name='isUserNumber' defaultChecked={isUserNumber} onChange={onHandleChangeBoard} autoComplete='off' />
      </div>

      <div className='inputBlockCustomStyle'>
        <div className='typeConfigTitle customTitle'>фон хедера</div>
        <div className='colorPickerCustom'>
          <ColorPicker
            className='colorPecerBackgroundColor colorPicker'
            animation='slide-up'
            color={backgroundColorHeader.color || backgroundColorHeader}
            alpha={backgroundColorHeader.alpha || 100}
            onChange={color => changeColor('backgroundColorHeader', color)}
          />
        </div>
      </div>

      <div className='inputBlockCustomStyle'>
        <div className='typeConfigTitle customTitle'>цвет текста хедера</div>
        <div className='colorPickerCustom'>
          <ColorPicker
            className='colorPecerBackgroundColor colorPicker'
            animation='slide-up'
            color={textColorHeader.color || textColorHeader}
            alpha={textColorHeader.alpha || 100}
            onChange={color => changeColor('textColorHeader', color)}
          />
        </div>
      </div>

      <div className='inputBlockCustomStyle'>
        <div className='typeConfigTitle customTitle'>фон доски</div>
        <div className='colorPickerCustom'>
          <ColorPicker
            className='colorPecerBackgroundColor colorPicker'
            animation='slide-up'
            color={backgroundColorRaitingBlock.color || backgroundColorRaitingBlock}
            alpha={backgroundColorRaitingBlock.alpha || 100}
            onChange={color => changeColor('backgroundColorRaitingBlock', color)}
          />
        </div>
      </div>

      <div className='inputBlockCustomStyle'>
        <div className='typeConfigTitle customTitle'>цвет текста блока пользователя</div>
        <div className='colorPickerCustom'>
          <ColorPicker
            className='colorPecerBackgroundColor colorPicker'
            animation='slide-up'
            color={textColorUserBlock.color || textColorUserBlock}
            alpha={textColorUserBlock.alpha || 100}
            onChange={color => changeColor('textColorUserBlock', color)}
          />
        </div>
      </div>

      <div className='inputBlockCustomStyle'>
        <div className='typeConfigTitle customTitle'>фон блока пользователя</div>
        <div className='colorPickerCustom'>
          <ColorPicker
            className='colorPecerBackgroundColor colorPicker'
            animation='slide-up'
            color={backgroundColorUserBlock.color || backgroundColorUserBlock}
            alpha={backgroundColorUserBlock.alpha || 100}
            onChange={color => changeColor('backgroundColorUserBlock', color)}
          />
        </div>
      </div>

      {children}
    </div>
  );

export default FormBoard;
