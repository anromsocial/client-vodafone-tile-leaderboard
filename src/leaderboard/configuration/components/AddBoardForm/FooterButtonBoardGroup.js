import React from 'react';
import { Button, Intent } from '@blueprintjs/core';

import { Modal } from '../../../helpers/Popover';

import './FooterButtonBoardGroup.css'

const FooterButtonBoardGroup = ({ board, usersQuantity, onCreateBoard, onUpdatedBoard, removeBoard, onCancel }) => {
  return !board.hasOwnProperty('isEditBoard') && !board.isEditBoard ? (
    <>
      <Button disabled={usersQuantity === 0 ? true : false} className='save btn buttonSuccess' intent={Intent.PRIMARY} text='Cоздать блок' onClick={onCreateBoard} />
      <Button className='cancel btn buttonDefault' text='Отмена' onClick={onCancel} />
    </>
  ) : (
    <div style={{ position: 'relative', zIndex: 101 }}>
      <Button disabled={usersQuantity === 0 ? true : false} className='save btn buttonSuccess' intent={Intent.PRIMARY} text='Cохранить блок' onClick={onUpdatedBoard} />
      <Button className='cancel btn buttonDefault' text='Отмена' onClick={onCancel} />
      <Modal onRemove={() => {
        removeBoard(board.id);
        // document.querySelector('#hover').style.display = 'none';
      }} showHover={true}>
        <Button className='cancel btn buttonDefault' text='Удалить блок'/>
      </Modal>
    </div>
  );
};

export default FooterButtonBoardGroup;