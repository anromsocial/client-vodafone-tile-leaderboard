import React from 'react';
import { SortableContainer, SortableElement } from 'react-sortable-hoc';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import TextTruncate from 'react-text-truncate';

import { Modal } from '../../../helpers/Popover';

import './BoardsList.css';

const BoardsList = ({ boards, onSortEnd, editBoard, onRemoveBoard }) => {
  const SortableItem = SortableElement(({ board }) => (
    <div className='boardListItem'>
      <div className='boardListItemTitle'>
        <TextTruncate line={2} truncateText={' ...'} text={board.title.toUpperCase()} />
      </div>
      <div className='boardListItemTools'>
        <FontAwesomeIcon
          title='редактировать'
          className='editButton2'
          style={{ cursor: 'pointer', marginRight: '10px' }}
          icon={faPencilAlt}
          onClick={() => window.parent.scrollTo(0, 720) || editBoard(board.id)}
        />
        <Modal onRemove={() => onRemoveBoard(board.id)}>
          <FontAwesomeIcon title='удалить' className='deleteButton2' style={{ cursor: 'pointer' }} icon={faTrashAlt} />
        </Modal>
      </div>
    </div>
  ));

  const SortableBoardList = SortableContainer(({ items }) => (
    <div className='boardListBlock'>
      {items.map((item, index) => (
        <SortableItem key={item.id} index={index} board={item} />
      ))}
    </div>
  ));

  return (
    boards.length > 0 && (
      <div className='configItemBlock'>
        <div className='headerBoardListBlock'>блоки лидеров</div>
        <SortableBoardList items={boards} onSortEnd={onSortEnd} lockAxis='y' distance={45} />
      </div>
    )
  );
};

export default BoardsList;
