const fs = require('fs')
const yaml = require('js-yaml')

// YAML local config
const localConfigFilePath = './config-local.yaml'

if (!fs.existsSync(localConfigFilePath)) {
    console.log('\x1b[31mERROR:\x1b[0m Local config file not found. Please copy the \x1b[1mconfig-local.template.yaml\x1b[0m')
    console.log('from your working dir, rename it to \x1b[1mconfig-local.yaml\x1b[0m and configure it!')
    process.exit(0)
}

// YAML global config
const configFilePath = './config.yaml'

if (!fs.existsSync(configFilePath)) {
    console.log('\x1b[31mERROR:\x1b[0m Config file not found. Please create \x1b[1mconfig.yaml\x1b[0m in your working dir')
    process.exit(0)
}

const localConfig = yaml.safeLoad(fs.readFileSync(localConfigFilePath))
const globalConfig = yaml.safeLoad(fs.readFileSync(configFilePath))

module.exports = {
    localConfig,
    globalConfig
}