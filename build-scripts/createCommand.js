module.exports = function createCommand(command, params) {
    const argStrings = Object.keys(params || {}).map(param => {

        const paramValue = params[param]
        let argString = ''

        //If paramValue is false or undefined - skip this argument
        if (paramValue !== false  && paramValue !== undefined) {
            // from here param will be "--paramName". If paramValue === true this is sufficient
            argString = param

            // if it's not false and nor true - expecting value
            if (paramValue !== true) {

                //now works this way to add quotes if explicitly put as type:string
                if (typeof paramValue === 'object') {
                    if (paramValue.value !== undefined){
                        if (!paramValue.type || paramValue.type === 'string') {
                            argString += `="${paramValue.value}"`
                        } else {
                            argString += '=' + paramValue.value
                        }
                    }
                } else {
                    argString += '=' + paramValue
                }
            }
        }

        return argString
    }).filter(string => !!string)

    return command + (argStrings.length ? ' ' + argStrings.join(' ') : '')
}