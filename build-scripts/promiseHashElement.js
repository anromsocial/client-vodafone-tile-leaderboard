const {hashElement} = require('folder-hash')

module.exports = function promiseHashElement(item, options) {
    return new Promise((resolve, reject) => {
        hashElement(item, options, (err, hash) => {
            if (err) {
                reject(err)
            } else {
                resolve(hash)
            }
        })
    })
}