const exec = require('sync-exec')

module.exports = function getGitInfo(disableUnsafeBuild = false) {

    const gitExists = exec('git --version').stdout.trim().indexOf('git version') !== -1;

    const gitInfo = {}

    if (!gitExists){
        gitInfo.__GIT__ = JSON.stringify('no-git')
        gitInfo.__GIT_COMMENT__ = JSON.stringify("Git was not installed on building machine")
    } else {

        const isRepo = exec('git branch').stderr.trim().indexOf('fatal: Not a git repository') === -1;

        if (!isRepo) {
            gitInfo.__GIT__ = JSON.stringify('no-repo')
            gitInfo.__GIT_COMMENT__ = JSON.stringify("Project is not versioned")
        } else {
            //imports required for revision info
            const moment = require('moment');

            // getting date
            const buildDate = moment().format('YYYY-MM-DD HH:mm:ss ZZ');

            // collecting git data
            const revision = exec('git rev-parse --short HEAD').stdout.trim();
            const branch = exec('git rev-parse --abbrev-ref HEAD').stdout.trim();
            const gitUserName = exec('git config user.name').stdout.trim();
            const gitUserEmail = exec('git config user.email').stdout.trim();
            const gitFullStatus = exec('git status').stdout.trim();
            const gitStatus = gitFullStatus.indexOf('nothing to commit') !== -1 ? 'OK' : "have changes :'(";

            gitInfo.__GIT__ = JSON.stringify('ok')
            gitInfo.__GIT_COMMENT__ = ""
            gitInfo.__BUILD_DATE__ = JSON.stringify(buildDate);
            gitInfo.__GIT_REVISION__ = JSON.stringify(revision);
            gitInfo.__GIT_BRANCH__ = JSON.stringify(branch);
            gitInfo.__GIT_USER_NAME__ = JSON.stringify(gitUserName);
            gitInfo.__GIT_USER_EMAIL__ = JSON.stringify(gitUserEmail);
            gitInfo.__GIT_STATUS__ = JSON.stringify(gitStatus);

            // failsafe to prevent production publishing without commit
            if (disableUnsafeBuild && gitStatus !== 'OK') {

                console.error('=============================================');
                console.error('Commit git changes before production build!!!'.toUpperCase());
                console.log(gitFullStatus);
                console.error('=============================================');
                process.exit(-1);
            }
        }
    }

    return gitInfo
}

