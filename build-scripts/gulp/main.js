const {task, series, src, dest} = require('gulp')

const shell = require('gulp-shell')
const rimraf = require('gulp-rimraf')
const zip = require('gulp-zip')
const fs = require('fs')
const path = require('path')
const beautify = require('json-beautify')
const args = require('yargs').argv
const ejs = require('gulp-ejs')
const {shake256} = require('js-sha3')
const yaml = require('js-yaml')
const getJiveAddonPath = require('../getJiveAddonPath')
const createCommand = require('../createCommand')
const promiseHashElement = require('../promiseHashElement')
const {localConfig, globalConfig} = require('../readConfig')

/**
 * COLLECTING INFO
 */
if (!args.mode) args.mode = 'prod'
const PROD = args.mode === 'prod'

const packageJson = JSON.parse(fs.readFileSync('./package.json'))
let version = packageJson.version || '0.1.0'
let build = ''

const {jivePath} = localConfig

const projectMeta = yaml.safeLoad(fs.readFileSync('./src/meta.yaml'))
const tileNames = Object.keys(globalConfig.tiles)

/**
 * GETTING BUILD NUMBER
 */

exports.getBuild = function getBuild() {
    const options = {files: {exclude: ['.DS_Store', '*.sh', '*.cmd', '*.bat']}}

    return Promise.all([
        promiseHashElement('./src', options),
        promiseHashElement('./public', options),
        promiseHashElement('./build-scripts', options),
        promiseHashElement('./gulpfile.js', options),
        promiseHashElement('./config.yaml', options),
        promiseHashElement('./package.json'),
        promiseHashElement('./package-lock.json'),
        promiseHashElement('./webpack.config.js'),
    ]).then(hashes => {
        //getting build stamp
        const stringHash = JSON.stringify(hashes)
        build = shake256(stringHash, 24)

        // adding it to version
        version += (('-' + build) + (PROD ? '' : '-dev'))

        //writing build to log
        const hashFile = beautify(hashes, null, 2, 100)
        if (!fs.existsSync('./logs')) fs.mkdirSync('./logs')
        if (!fs.existsSync('./logs/buildstamps')) fs.mkdirSync('./logs/buildstamps')

        fs.writeFileSync(`./logs/buildstamps/${build}.json`, hashFile)
    })
}

/**
 * WEBPACK TASKS
 */

exports.runWebpack = function runWebpack() {
    return src('./', {read: false}).pipe(shell(createCommand('webpack', {
        "--progress": true,
        "--env.mode": PROD ? 'production' : 'development',
        "--env.local": args.local,
        "--watch": args.watch,
        "--env.appVersion": JSON.stringify(version + (args.local ? '-local' : '')),
        "--env.force": args.force || false
    })))
}

exports.runWebpackServer = function runWebpackServer() {
    return src('./', {read: false}).pipe(shell(createCommand('webpack-dev-server', {
        "--hot": true,
        "--env.local": args.local,
        "--env.hot": "true",
        "--env.mode": PROD ? 'production' : 'development',
        "--env.appVersion": JSON.stringify(version + '-hot')
    })))
}

/**
 * CREATE CONFIG FILES
 */

exports.config = function config(cb) {

    //jiveclientconfiguration.json
    const JCC = projectMeta.jiveClientConfiguration

    const {
        uuid,
        displayName,
        description,
        icon_16,
        icon_48,
        icon_128
    } = globalConfig.addon

    JCC.extensionInfo = {
        ...JCC.extensionInfo,
        id: uuid,
        uuid,
        name: displayName,
        description,
        icon_16,
        icon_48,
        icon_128
    }

    fs.writeFileSync('./jiveclientconfiguration.json', beautify(JCC, null, 4, 80))


    tileNames.forEach(tileName => {
        if (fs.existsSync(`./src/${tileName}`)) {
            //tile/{tile-name}/definition.json
            const tileMeta = globalConfig.tiles[tileName]
            let definitionJson = yaml.safeLoad(fs.readFileSync(`./src/${tileName}/meta.yaml`))

            let {
                displayName,
                description,
                pageTypes,
                icons,
                configFeatures,
                viewFeatures
            } = tileMeta

            definitionJson = {
                ...definitionJson,
                displayName,
                description,
                pageTypes,
                icons
            }

            configFeatures = configFeatures || []
            definitionJson.config = `/${tileName}/configuration.html?features=${configFeatures.join(',')}`

            viewFeatures = viewFeatures || []
            definitionJson.view = `/${tileName}/view.html?features=${viewFeatures.join(',')}`

            fs.writeFileSync(`./tiles/${tileName}/definition.json`, beautify(definitionJson, null, 4, 80))
        }
    })

    cb()
}

/**
 * JIVE BUILD APP
 */
exports.jiveBuildApp = function jiveBuildApp() {
    return src('./', {read: false}).pipe(shell('jive-sdk build add-on --apphosting="jive"'))
}

/**
 * PACKAGING
 */

function deleteZip() {
    return src('./extension.zip').pipe(rimraf())
}

function newZip() {
    const name = globalConfig.addon.displayName
        .toLowerCase()
        .replace(/\s/g, '-')
        .replace(/---/g, '-')

    const tileMeta = JSON.parse(fs.readFileSync('./extension_src/meta.json'))
    tileMeta.version = version
    fs.writeFileSync('./extension_src/meta.json', beautify(tileMeta, null, 4, 80))

    src('./extension_src/**').pipe(zip(`extension.zip`)).pipe(dest('./'))

    const zipFileName = `${name}-${version}.zip`

    console.log('Saving package as', zipFileName)

    return src('./extension_src/**').pipe(zip(zipFileName)).pipe(dest('./dist'))
}

exports.rename = series(deleteZip, newZip)

/**
 * COPY HTML
 */

exports.copyHTMLHot = function copyHTMLHot() {
    if (jivePath) {

        const addonPath = getJiveAddonPath(jivePath, tileNames[0])

        const promises = tileNames.map(tileName => {
            if (fs.existsSync('./src/' + tileName)) {

                const tilePath = path.resolve(addonPath, 'tiles', tileName)

                return new Promise((resolve, reject) => src([
                    `./src/${tileName}/templates/view.html`,
                    `./src/${tileName}/templates/configuration.html`,
                    `./src/${tileName}/templates/dom_actions.html`
                ]).pipe(ejs({
                    date: version,
                    jsUrl: localConfig.devServerURL,
                    cssUrl: false
                })).pipe(dest(tilePath))
                    .on('end', resolve)
                    .on('error', reject))

            } else {
                return Promise.resolve()
            }
        })

        return Promise.all(promises)
    } else {
        return Promise.reject(new Error('jive path is not specified!'))
    }
}

exports.copyHTMLLocal = function copyHTMLLocal() {
    if (jivePath) {

        const addonPath = getJiveAddonPath(jivePath, tileNames[0])

        const promises = tileNames.map(tileName => {
            if (fs.existsSync('./src/' + tileName)) {

                const tilePath = path.resolve(addonPath, 'tiles', tileName)

                return new Promise((resolve, reject) => src([
                    `./src/${tileName}/templates/view.html`,
                    `./src/${tileName}/templates/configuration.html`,
                    `./src/${tileName}/templates/dom_actions.html`
                ]).pipe(ejs({
                    date: version,
                    jsUrl: 'javascripts/',
                    cssUrl: 'stylesheets/'
                })).pipe(dest(tilePath))
                    .on('end', resolve)
                    .on('error', reject))

            } else {
                return Promise.resolve()
            }
        })

        return Promise.all(promises)
    } else {
        return Promise.reject(new Error('jive path is not specified!'))
    }
}

exports.copyHTML = function copyHTML() {
    const promises = tileNames.map(tileName => {
        if (fs.existsSync('./src/' + tileName)) {

            return new Promise((resolve, reject) => src([
                `./src/${tileName}/templates/view.html`,
                `./src/${tileName}/templates/configuration.html`,
                `./src/${tileName}/templates/dom_actions.html`
            ]).pipe(ejs({
                date: version,
                jsUrl: 'javascripts/',
                cssUrl: 'stylesheets/'
            })).pipe(dest(`./tiles/${tileName}/public/`))
                .on('end', resolve)
                .on('error', reject))

        } else {
            return Promise.resolve()
        }
    })

    return Promise.all(promises)
}