const fs = require('fs')
const path = require('path')
const yaml = require('js-yaml')

module.exports = function(jivePath, tileName){

    // getTile UUID
    const globalConfig = yaml.safeLoad(fs.readFileSync(path.join(__dirname, '..', 'config.yaml')))
    const addonUUID = globalConfig.addon.uuid

    // create path base
    const jiveAddonPath = path.join(jivePath, 'target', 'jiveHome', 'www', 'resources', 'add-ons', addonUUID)

    if (!fs.existsSync(jiveAddonPath)){
        console.error(`\x1b[31m==================\nBUILD SCRIPT ERROR!\nTo use this command you have to upload tile to jive as a .zip package at least once\n==================`)
        process.exit(-1)
    }

    //get hash id
    let addonRevisionHash = ''

    const dir = fs.readdirSync(jiveAddonPath)

    let tileDirs = []

    //list all folders in addon dir
    dir.forEach(item => {
        const stats = fs.statSync(path.join(jiveAddonPath, item))

        //if directory
        if (stats.isDirectory()){
            if (fs.existsSync(path.join(jiveAddonPath, item, 'tiles'))) {
                const tileDir = fs.readdirSync(path.join(jiveAddonPath, item, 'tiles'))

                // if contains "tiles/{tileName}"
                tileDir.forEach(folderName => {
                    if (folderName === tileName) {
                        tileDirs.push({
                            addonRevisionHash: item,
                            folderCreationDate: Math.round(stats.birthtimeMs)
                        })
                    }
                })
            }
        }
    })


    if (!tileDirs.length) {
        console.error('ERROR: directory not found! First time upload the add-on manually!')
        process.exit(-1);
    }

    //sort by folderCreationDate, descending
    tileDirs.sort((a, b) => b.folderCreationDate - a.folderCreationDate)

    addonRevisionHash = tileDirs[0].addonRevisionHash

    return path.join(jiveAddonPath, addonRevisionHash)
}