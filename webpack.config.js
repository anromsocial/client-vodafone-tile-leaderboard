const webpack = require('webpack')
const path = require('path')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const ExtractTextPlugin = require("extract-text-webpack-plugin")
const url = require('url')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

//local imports
const getJiveAddonPath = require('./build-scripts/getJiveAddonPath')
const getGitInfo = require('./build-scripts/getGitInfo')
const {globalConfig, localConfig} = require('./build-scripts/readConfig.js')

const {devMiddleware, jivePath, devServerURL} = localConfig
const {middleware} = globalConfig

const tileNames = Object.keys(globalConfig.tiles)

function addTrailSlash (address) {
    return address.substr(-1) !== '/' ? address + '/' : address
}

function isVendorJS(module){
    if (module.resource && (/^.*\.(css|scss)$/).test(module.resource)) return false
    return module.context && module.context.includes('node_modules')
}

//main webpack configuration
module.exports = function (env = {}) {

    console.log('env:', env)
    console.log('Local config:', localConfig)

    /**
     * ENVIRONMENT VARIABLES
     */
    const PROD = env.mode !== 'development'
    const DEV = env.mode === 'development'
    const JIVE_PATH = jivePath || false
    const HOT = (env.hot && jivePath) || false
    const LOCAL = (env.local && jivePath) || false
    const HOT_URL = devServerURL ? addTrailSlash(devServerURL) : 'http://localhost:8081/'
    const MW_URL = middleware ? (PROD ? middleware : (devMiddleware || middleware)) : false
    const VERSION = env.appVersion || "no-version"
    const FORCE = env.force //allows to skip git commit check before production build


    /**
     * PLUGINS
     */
    let definePluginConfig = {
        "process.env": {
            NODE_ENV: JSON.stringify(env.mode),
            APP_NAME: JSON.stringify(tileNames[0]),
            VERSION: JSON.stringify(VERSION)
        }
    }

    if (MW_URL) {
        definePluginConfig.MW_URL = MW_URL
    }

    //add git info (comment if you have some problems with it)
    definePluginConfig = Object.assign(definePluginConfig, getGitInfo(!FORCE && PROD))

    const plugins = [
        new webpack.DefinePlugin(definePluginConfig),

        //remove all redundant locales from moment.js
        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en-gb|en-us|ru/),

        new webpack.optimize.CommonsChunkPlugin({
            name: 'view-vendor',
            chunks: ["view"],
            minChunks: isVendorJS
        }),

        new webpack.optimize.CommonsChunkPlugin({
            name: 'config-vendor',
            chunks: ["configuration"],
            minChunks: isVendorJS
        }),

        new webpack.optimize.CommonsChunkPlugin({
            name: 'common-vendor',
            chunks: ["config-vendor", "view-vendor"],
            minChunks: 2
        })
    ]

    if (PROD) {
        plugins.push(new BundleAnalyzerPlugin({
            analyzerMode: 'static',
            reportFilename: path.join(__dirname, 'webpack-analyzer-report.html'),
            openAnalyzer: false
        }))

        plugins.push(new UglifyJsPlugin({
            uglifyOptions: {
                compress: {
                    pure_funcs: ['console.log']
                }
            }
        }))

        plugins.push(new webpack.LoaderOptionsPlugin({minimize: true}))

        //babelOptions.presets.push('react-optimize')
    }

    if (DEV) {
        plugins.push(new webpack.SourceMapDevToolPlugin({
            exclude: /vendor/
        }))
    }

    /**
     * BABEL CONFIGURATION
     */

    const babelOptions = {
        babelrc: false,
        presets: [
            ["@babel/preset-env", {
                modules: false,
                useBuiltIns: "usage"
            }],
            "@babel/preset-react",
            "@babel/preset-flow"
        ],
        plugins: [
            "@babel/plugin-transform-runtime", // add regenerator, Promise and Symbol
            "@babel/plugin-proposal-function-bind", // ::this.func(), []::map
            "@babel/plugin-proposal-export-default-from", // export default from './TileView'
            "@babel/plugin-proposal-optional-chaining", // foo?.bar?.baz
            "@babel/plugin-proposal-nullish-coalescing-operator", // foo?.bar ?? defaultValue (not '', 0, false)
            "@babel/plugin-proposal-class-properties" // static class props
        ]
    }

    /**
     * GETTING HMR READY
     */
    let hotPath = '', cssLoader

    if (LOCAL) {
        if (!hotPath) hotPath = path.resolve(getJiveAddonPath(JIVE_PATH, tileNames[0]), 'tiles', tileNames[0])
    }

    /**
     * CSS LOADER CONFIG
     */

    const CSS_CONFIG = [
        {
            loader: 'css-loader',
            options: {localIdentName: "[name]-[local]--[hash:base64:5]"}
        }, {
            loader: 'sass-loader',
            options: {
                outputStyle: 'expanded',
                sourceComments: true
            }
        }, {
            loader: 'postcss-loader',
            options: {
                plugins: [
                    require('autoprefixer')
                ]
            }
        }
    ]

    if (HOT) {
        if (!hotPath) hotPath = path.resolve(getJiveAddonPath(JIVE_PATH, tileNames[0]), 'tiles', tileNames[0])

        cssLoader = [
            'style-loader',
            ...CSS_CONFIG
        ]

        babelOptions.plugins.push('react-hot-loader/babel')

        plugins.push(new webpack.NamedModulesPlugin())

    } else {
        cssLoader = ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [...CSS_CONFIG]
        })

        plugins.push(new ExtractTextPlugin('../stylesheets/[name].css'))
    }

    const viewEntryPath = path.join(__dirname, 'src', tileNames[0], 'view.js')
    const confEntryPath = path.join(__dirname, 'src', tileNames[0], 'configuration.js')

    const hotUrlObj = url.parse(HOT_URL)

    /**
     * ACTUAL CONFIG OBJECT
     */

    const webpackConfigObject = {
        devServer: {
            https: hotUrlObj.protocol === 'https:',
            port: hotUrlObj.port,
            host: hotUrlObj.hostname,
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        },

        devtool: false,

        resolve: {
            // new babel tries to resolve paths in symlinked (npm link) modules to the link source
            // parent dir. This makes it resolve it to the link target dir (local node_modules)
            symlinks: false,
            extensions: ['.webpack.js', '.web.js', '.js', '.min.js', '.jsx'],
            alias: {
                // some dependencies still use core-js explicitly. This is to avoid duplicate imports
                "core-js/library": path.join(__dirname, "node_modules", "core-js")
            }
        },

        entry: {
            view: HOT ? ['react-hot-loader/patch', viewEntryPath] : viewEntryPath,
            configuration: HOT ? ['react-hot-loader/patch', confEntryPath] : confEntryPath
        },

        output: {
            path: LOCAL ? path.join(hotPath, 'javascripts') : path.join(__dirname, 'tiles', tileNames[0], 'public', 'javascripts'),
            publicPath: HOT ? HOT_URL : '',
            filename: '[name].js'
        },

        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: [{
                        loader: 'babel-loader',
                        options: babelOptions
                    }]
                }, {
                    test: /\.(jpg|jpeg|png|gif|svg)$/i,
                    use: [{
                        loader: 'url-loader',
                        options: {
                            name: 'images/[name].[ext]',
                            limit: 32 * 1024 // 32kb - data-url limit for IE
                        }
                    }]
                }, {
                    test: /.(css|sass|scss)$/i,
                    use: cssLoader
                }, {
                    test: /\.(woff|woff2|ttf|eot|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                    use: [{
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            publicPath: '../../../fonts',
                            outputPath: '../../../../public/fonts'
                        }
                    }]
                }
            ]
        },

        plugins,

        externals: {
            "jive": "jive",
            "jive/gadgets": "gadgets",
            "jive/jquery": "jQuery",
            "jive/osapi": "osapi"
        }
    }

    return webpackConfigObject
}
